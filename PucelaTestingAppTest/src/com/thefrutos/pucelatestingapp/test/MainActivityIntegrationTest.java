package com.thefrutos.pucelatestingapp.test;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.thefrutos.pucelatestingapp.R;
import com.thefrutos.pucelatestingapp.activities.DetailActivity;
import com.thefrutos.pucelatestingapp.activities.MainActivity;

import static android.app.Instrumentation.*;

public class MainActivityIntegrationTest extends ActivityInstrumentationTestCase2 {

    private MainActivity activity;

    public MainActivityIntegrationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        activity = (MainActivity) getActivity();
    }

    public void testThatSettedAnAmountGoToDetailActivityAndShowsThatAmountConverted() {
        ActivityMonitor monitor = getInstrumentation().addMonitor(DetailActivity.class.getName(),null, false);
        final EditText amountField = (EditText) activity.findViewById(R.id.currency_input_field);
        final Button convertButton = (Button) activity.findViewById(R.id.convert_cash_button);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                amountField.setText("10");
                convertButton.performClick();
            }
        });
        DetailActivity startedActivity = (DetailActivity) monitor.waitForActivityWithTimeout(5000);
        assertNotNull(startedActivity);
        TextView convertAmountText = (TextView) startedActivity.findViewById(R.id.amount_converted);
        assertEquals("7.4", convertAmountText.getText());
    }
}

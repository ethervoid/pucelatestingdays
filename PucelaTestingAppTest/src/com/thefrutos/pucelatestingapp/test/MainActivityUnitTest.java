package com.thefrutos.pucelatestingapp.test;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.RadioButton;

import com.thefrutos.pucelatestingapp.R;
import com.thefrutos.pucelatestingapp.activities.MainActivity;

public class MainActivityUnitTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;

    public MainActivityUnitTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                MainActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    public void testRadioOfEurIsMarkedByDefault() {
        RadioButton radioEurs = (RadioButton) activity.findViewById(R.id.eurs_radio_button);
        assertTrue("EUR radio button not active by default", radioEurs.isChecked());
    }
}

package com.thefrutos.test;

import com.thefrutos.CurrencyConverter;

import junit.framework.TestCase;

public class CurrencyConverterTest extends TestCase {

    private CurrencyConverter converter;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        converter = new CurrencyConverter();
    }

    public void testShouldConvertFromEurToDollar() {
        float amountConverted = converter.convertFromDollarToEur(10);
        assertEquals(7.4f, amountConverted);
    }
}

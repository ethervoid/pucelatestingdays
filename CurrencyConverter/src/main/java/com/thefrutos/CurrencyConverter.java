package com.thefrutos;

public class CurrencyConverter {

    private static final float DOLAR_EUR_CONVERSION = 0.74f;

    public float convertFromDollarToEur(float amount) {
        return amount * DOLAR_EUR_CONVERSION;
    }
}

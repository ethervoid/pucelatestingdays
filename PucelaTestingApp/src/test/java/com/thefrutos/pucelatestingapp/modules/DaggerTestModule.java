package com.thefrutos.pucelatestingapp.modules;

import com.thefrutos.CurrencyConverter;
import com.thefrutos.pucelatestingapp.module.DaggerModule;
import com.thefrutos.pucelatestingapp.DetailActivityRobolectricTest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@Module(
        injects = {DetailActivityRobolectricTest.class},
        includes = DaggerModule.class,
        overrides = true
)
public class DaggerTestModule {

    @Singleton
    @Provides
    public CurrencyConverter providesCurrencyConverter() {
        return mock(CurrencyConverter.class);
    }
}

package com.thefrutos.pucelatestingapp;

import com.thefrutos.pucelatestingapp.modules.DaggerTestModule;

import org.robolectric.Robolectric;

import java.util.List;

public class TestMyApplication extends MyApplication {

    @Override
    protected List<Object> getModules() {
        List<Object> modules = super.getModules();
        modules.add(new DaggerTestModule());
        return modules;
    }

    public static <T> T injectMocks(T object) {
        TestMyApplication app = (TestMyApplication) Robolectric.application;
        return app.inject(object);
    }
}

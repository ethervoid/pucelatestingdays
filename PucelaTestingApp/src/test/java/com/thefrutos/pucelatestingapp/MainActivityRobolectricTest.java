package com.thefrutos.pucelatestingapp;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.thefrutos.pucelatestingapp.activities.DetailActivity;
import com.thefrutos.pucelatestingapp.R;
import com.thefrutos.pucelatestingapp.activities.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.robolectric.Robolectric.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class MainActivityRobolectricTest {

    private MainActivity activity;
    private Button convertButton;
    private EditText amountField;

    @Before
    public void setUp() {
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        convertButton = (Button) activity.findViewById(R.id.convert_cash_button);
        amountField = (EditText) activity.findViewById(R.id.currency_input_field);
    }

    @Test
    public void whenTheActivityIsLoadedTheEurRadioButtonShouldBeChecked() {
        RadioButton radioButton = (RadioButton) activity.findViewById(R.id.eurs_radio_button);
        assertThat(radioButton.isChecked(), equalTo(true));
    }

    @Test
    public void whenTheConvertButtonIsPressedTheDetailActivityIsCalled() {
        Button convertButton = (Button) activity.findViewById(R.id.convert_cash_button);
        convertButton.performClick();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(startedIntent);
        assertThat(shadowIntent.getComponent().getClassName(), equalTo(DetailActivity.class.getName()));
    }

    @Test
    public void whenTheConvertButtonIsClickedTheAmountIsSentToTheNextActivity() {
        amountField.setText("10");
        convertButton.performClick();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(startedIntent);
        assertThat((String) shadowIntent.getExtras().get("amount"), equalTo("10"));
    }
}

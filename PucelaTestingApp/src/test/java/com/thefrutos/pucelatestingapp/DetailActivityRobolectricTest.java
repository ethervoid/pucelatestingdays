package com.thefrutos.pucelatestingapp;

import android.content.Intent;
import android.os.Bundle;

import com.thefrutos.CurrencyConverter;
import com.thefrutos.pucelatestingapp.activities.DetailActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowActivity;

import javax.inject.Inject;

import static com.thefrutos.pucelatestingapp.TestMyApplication.injectMocks;
import static org.mockito.Matchers.anyFloat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.Robolectric.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class DetailActivityRobolectricTest {

    private DetailActivity activity;

    @Inject
    CurrencyConverter converterMock;

    @Before
    public void setUp() {
        injectMocks(this);
        Intent intent = new Intent(Robolectric.getShadowApplication().getApplicationContext(), DetailActivity.class);
        intent.putExtra("amount", "10");
        activity = Robolectric.buildActivity(DetailActivity.class).withIntent(intent).create().get();
    }

    @Test
    public void whenActivityIsCreatedTheAmountIsConverted() {
        ShadowActivity shadowActivity = shadowOf(activity);
        shadowActivity.callOnCreate(new Bundle());
        verify(converterMock, atLeastOnce()).convertFromDollarToEur(anyFloat());
    }
}

package com.thefrutos.pucelatestingapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.thefrutos.pucelatestingapp.R;

public class MainActivity extends Activity {

    private Button convertButton;
    private EditText amountField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convertButton = (Button) findViewById(R.id.convert_cash_button);
        convertButton.setOnClickListener(convertButtonClickListener());
        amountField = (EditText) findViewById(R.id.currency_input_field);
    }

    private View.OnClickListener convertButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailActivityIntent = new Intent(MainActivity.this, DetailActivity.class);
                detailActivityIntent.putExtra("amount", amountField.getText().toString());
                startActivity(detailActivityIntent);
            }
        };
    }

}

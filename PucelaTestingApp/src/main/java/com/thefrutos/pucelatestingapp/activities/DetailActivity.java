package com.thefrutos.pucelatestingapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.thefrutos.CurrencyConverter;
import com.thefrutos.pucelatestingapp.MyApplication;
import com.thefrutos.pucelatestingapp.R;
import com.thefrutos.pucelatestingapp.module.DaggerModule;

import javax.inject.Inject;

import dagger.ObjectGraph;

public class DetailActivity extends Activity {

    private TextView convertedText;

    @Inject
    CurrencyConverter converter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        ((MyApplication) getApplication()).inject(this);
        convertedText = (TextView) findViewById(R.id.amount_converted);
        float amount = getAmount();
        float convertedAmount = converter.convertFromDollarToEur(amount);
        convertedText.setText(String.valueOf(convertedAmount));
    }

    private float getAmount() {
        String amountString = getIntent().getExtras().getString("amount");
        return Float.valueOf(amountString);
    }
}

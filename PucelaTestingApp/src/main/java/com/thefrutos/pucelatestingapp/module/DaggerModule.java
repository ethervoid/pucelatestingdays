package com.thefrutos.pucelatestingapp.module;

import android.content.Context;

import com.thefrutos.CurrencyConverter;
import com.thefrutos.pucelatestingapp.activities.DetailActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        injects = {DetailActivity.class}
)
public class DaggerModule {

    private final Context context;

    public DaggerModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public CurrencyConverter providesCurrencyConverter() {
        return new CurrencyConverter();
    }
}

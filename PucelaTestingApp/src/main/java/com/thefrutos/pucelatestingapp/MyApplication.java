package com.thefrutos.pucelatestingapp;

import android.app.Application;
import android.content.Context;

import com.thefrutos.pucelatestingapp.module.DaggerModule;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;

import static dagger.ObjectGraph.*;

public class MyApplication extends Application {

    public static MyApplication from (Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph = create(getModules().toArray());
    }

    public <T> T inject(T object) {
        return objectGraph.inject(object);
    }

    protected List<Object> getModules() {
        List<Object> modules = new ArrayList<Object>(1);
        modules.add(new DaggerModule(this));
        return modules;
    }
}